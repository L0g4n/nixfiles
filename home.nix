{ pkgs, config, ...}:

{
  programs.home-manager.enable = true;

  home.username = (builtins.getEnv "USER");
  home.homeDirectory = (builtins.getEnv "HOME");

  home.stateVersion = "22.05";

  home.packages = with pkgs; [
    # interactive shell
    fish

    # cli tools
    ripgrep
    exa
    fd
    bat

    # pdf
    qpdf

    # spellcheck
    hunspell
    hunspellDicts.en_US
    hunspellDicts.de_DE

    gnupg

    ctags

    neovim
    tmux

    # prompt
    starship

    # static site generator
    zola

    # rust tools
    rust-analyzer
    cargo-release

    ansible


    # stuff for embedded system sec
    socat
    htop

    python3
    python39Packages.setuptools
  ];

  # raw config files
  xdg.configFile."alacritty/alacritty.yml".source = ./alacritty.yml;

  home.file.".zshrc".source = ./zshrc;
  
  xdg.configFile."nvim/init.lua".source = ./nvim-init.lua;

  # tmux
  home.file.".tmux.conf".source = ./tmux.conf;

  # Git config using Home Manager modules
  programs.git = {
    enable = true;
    userName = "L0g4n";
    userEmail = "yannik.klubertanz@posteo.de";
    aliases = {
      st = "status";
    };
    ignores = [ 
      "*~"              # vim temporary files
      ".DS_Store"       # macOS finder temp files
      "tags*"           # ctags
    ];
    extraConfig = {
      core = {
        editor = "nvim";
      };
    };
  };

  programs.starship = {
    enable = true;
    settings = {
      add_newline = false;
      line_break = {
        disabled = true;
      };
    };
  };

  programs.zsh = {
    enable = true;
    enableCompletion = false;
  };
  
  programs.fish = {
    enable = true;
    shellInit = ''
      fish_vi_key_bindings
    '';
    functions = {
      fish_greeting = {
        description = "Greeting to show when starting a fish shell";
        body = "";
      };
      mkdcd = {
        description = "Make a directory tree and enter it";
        body = "mkdir -p $argv[1]; and cd $argv[1]";
      };
      ls = {
        body = "exa $argv";
      };
      cat = {
        body = "bat $argv";
      };
      
    };
    shellAliases = {
      vim  = "nvim";
      grep = "rg";
      find = "fd";
    };
  };
}
