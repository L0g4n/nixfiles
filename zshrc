export EDITOR=nvim
export TERM=xterm-256color
export CARGO_TARGET_DIR="$HOME/rust-artifacts"

case "$TERM" in
linux|xterm*|rxvt*)
  export PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME%%.*}: ${PWD##*/}\007"'
  ;;
screen*)
  export PROMPT_COMMAND='echo -ne "\033k${HOSTNAME%%.*}: ${PWD##*/}\033\\" '
  ;;
*)
  ;;
esac

[[ -f $HOME/.cargo/env ]] && source $HOME/.cargo/env

# drop into fish if the parent process is NOT fish and bash or zsh are not executed with `bash -c "cmd"`
if [[ $(ps -p $PPID) != "fish" && -z ${BASH_EXECUTION_STRING} && -z ${ZSH_EXECUTION_STRING} ]] 
then
    exec fish
fi

